// ignore: unused_import
import 'package:app_system/dashboardScreen.dart';
import 'package:app_system/homeScreen.dart';
import 'package:app_system/welcomeScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter App',

      debugShowCheckedModeBanner: false,
      home: Home(),
     
    );
  }
}
