import 'package:app_system/dashboardScreen.dart';
import 'package:flutter/material.dart';

class Feature2 extends StatefulWidget {
  const Feature2({Key? key}) : super(key: key);

  @override
  State<Feature2> createState() => _Feature2State();
}

class _Feature2State extends State<Feature2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feature 2'),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => const Dashboard()));
          },
        ),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.settings), onPressed: () {}),
        ],
      ),
    );
  }
}
